package org.soprasteria.formation.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by lmarchau on 16/06/2016.
 */
public class SimpleFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFilter.class);
    public static final String WITH_SIMPLE_FILTER = " with SimpleFilter";

    private ServletContext context;

    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Init de SimpleFilter");
        context = filterConfig.getServletContext();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    }

    public void destroy() {
    }

    private class CharResponseWrapper extends HttpServletResponseWrapper {

        private CharArrayWriter output;


        public CharResponseWrapper(HttpServletResponse response) {
            super(response);
            output = new CharArrayWriter();
        }

        public PrintWriter getWriter() {
            return new PrintWriter(output);
        }

        public String toString() {
            return output.toString();
        }

    }

}
