package org.soprasteria.formation.servlet;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Constant response servlet
 * @author sbourret
 *
 */
public class ConstantResponseServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Constant response
	 */
	private String message;
	
	public ConstantResponseServlet(String message) {
		super();
		this.message = message;
	}

	private String buildMessage(String path) {
		return String.format(message, path, LocalDateTime.now());
	}

	/**
	 * Provide constant resposne
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException
	    {
	    }
	
}
