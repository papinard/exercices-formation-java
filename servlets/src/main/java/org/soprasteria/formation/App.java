package org.soprasteria.formation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.soprasteria.formation.server.HttpSimpleServer;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) throws Exception {
        HttpSimpleServer httpServer = new HttpSimpleServer();
        LOGGER.info("Exéction du TP Servlets && Filters");
        httpServer.startServers();

//        httpServer.stopServers();

    }
}
