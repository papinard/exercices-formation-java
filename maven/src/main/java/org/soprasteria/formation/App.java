package org.soprasteria.formation;

import com.github.lalyos.jfiglet.FigletFont;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String asciiArt = FigletFont.convertOneLine("Maven's on fire !!");
        System.out.println(asciiArt);
    }
}
