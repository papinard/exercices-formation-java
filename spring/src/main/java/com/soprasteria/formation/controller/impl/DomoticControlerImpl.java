package com.soprasteria.formation.controller.impl;

import com.soprasteria.formation.appareil.Variator;
import com.soprasteria.formation.controller.DomoticControler;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lmarchau on 14/06/2016.
 */
public class DomoticControlerImpl implements DomoticControler {

    private List<Variator> variators;

    private List<Variator> otherVariators;

    public void incrementeThermostat() {
        variators.forEach(v -> v.incrementeThermostat());
        otherVariators.forEach(v -> v.incrementeThermostat());
    }

    public void decrementeThermostat() {
        variators.forEach(v -> v.decrementeThermostat());
        otherVariators.forEach(v -> v.decrementeThermostat());
    }


}
