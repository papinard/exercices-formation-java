package com.soprasteria.formation.appareil;



/**
 * classe repr�sentant un appareil thermostat�
 *
 * @author Sopra Group
 * @version 1.0
 */
public class AppareilThermostate extends AppareilElectrique implements Variator {

	/**
	 * thermostat de l'appareil
	 */
	private int thermostat;

	/**
	 * valeur maximum du thermostat (initialis�e � 12)
	 */
	private int valeurMaxThermostat = 12;


	public AppareilThermostate() {
	}

	public AppareilThermostate(String nom, String marque, String modele, int puissance, int thermostat) {
		super(nom, marque, modele, puissance);
		this.thermostat = thermostat;
	}

	/**
	 * Constructeur de la classe AppareilThermostate, 
	 * appel� lors d'un new AppareilThermostate(...)
	 * 
	 * @param pNom le nom de l'appareil
	 * @param pMarque la marque de l'appareil thermostate
	 * @param pModele le modele de l'appareil thermostate
	 * @param pPuissance la puissance de l'appareil thermostate
	 * @see AppareilElectrique(String, String, int)
	 */
	public AppareilThermostate(String pNom, String pMarque, String pModele, int pPuissance) {
		// appel du constructeur de la classe AppareilThermostate
		super(pNom, pMarque, pModele, pPuissance);
	}


	/**
	 *  getter de l'attribut thermostat
	 * @return la valeur du thermostat
	 */
	public int getThermostat() {
		return thermostat;
	}

	/**
	 * setter de l'attribut thermostat
	 * @param pThermostat la valeur du thermostat
	 */
	public void setThermostat(int pThermostat) {
		thermostat = pThermostat;
	}

	/**
	 * getter de l'attribut valeurMaxThermostat
	 * @return la valeur maximum du thermostat
	 */
	public int getValeurMaxThermostat() {
		return valeurMaxThermostat;
	}


	/**
	 * setter de l'attribut valeurMaxThermostat
	 * @param pValeurMaxThermostat la valeur maximum du thermostat
	 */
	public void setValeurMaxThermostat(int pValeurMaxThermostat) {
		valeurMaxThermostat = pValeurMaxThermostat;
	}


	/**
	 * S'arr�te en remettant le thermostat � z�ro
	 * @see AppareilElectrique#arreter()
	 */
	@Override
	public void arreter(){
		System.out.println("Variator: Arrêt de " + getNom());
		thermostat = 0;
		super.arreter();
	}

	/**
	 * methode permettant d'incrementer le thermostat
	 * affichage d'un message d'erreur si le thermostat est d�j� � son maximum
	 */	public void incrementeThermostat() {
		 // test si le thermostat est � son maximum
		 if (thermostat == valeurMaxThermostat){
			 // affichage d'un message
			 System.out.println("Thermostat deja a son maximum !");
		 }
		 else{
			 // incrementation du thermostat
			 System.out.println("Incremente thermostat pour " + getNom());
			 thermostat++;
		 }
	 }


	 /**
	  * methode permettant de d�cr�menter le thermostat
	  * affichage d'un message d'erreur si le thermostat est d�j� � 0
	  */
	 public void decrementeThermostat() {
		 // test si le thermostat est � 0
		 if (thermostat == 0){
			 // affichage d'un message
			 System.out.println("Thermostat deja a 0 !");
		 }
		 else{
			 // incrementation du thermostat
			 System.out.println("Decremente thermostat pour " + getNom());
			 thermostat--;
		 }
	 }
	 /**
	  * toString affiche les attributs
	  * @see AppareilElectrique#toString()
	  */
	 @Override
	 public String toString(){
		 StringBuffer buffer = new StringBuffer();
		 buffer.append(super.toString());  // affichages des attributs communs avec AppareilElectrique
		 buffer.append(", thermostat = ").append(thermostat);
		 buffer.append(", valeurMaxThermostat = ").append(valeurMaxThermostat);
		 return buffer.toString();

	 }

	 /**
	  * @return la consommation de l'appareil thermostate <ul>
	  * <li> puissance * thermostat si l'appareil est d�marr� </li>
	  * <li> 0 si l'appareil est arret� </li>
	  * </ul>
	  * 
	  */
	 @Override
	 public int getConsommation(){
		 // retourne thermostat*consommation de l'appareil electrique
		 return super.getConsommation() * thermostat;
	 }


	 /**
	  * Surcharge de equals
	  * @see AppareilElectrique#equals(Object)
	  */
	 @Override
	 public boolean equals (Object o){
		 // retour vrai si m�me classe, que les attributs de AppareilElectrique sont identiques et que le thermostat est le m�me
		 boolean retour = super.equals(o) 
		 && (this.getThermostat() == ((AppareilThermostate)o).getThermostat());
		 return retour;
	 }

}
