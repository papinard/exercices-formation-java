package com.soprasteria.formation.appareil;

/**
 * classe repr�sentant un appareil securis�
 */
public class AppareilSecurise extends AppareilThermostate {
	/**
	 * securite de l'appareil enclench�e ?
	 */
	private boolean securiteEnclenchee;
	
	/**
	 * Constructeur de la classe AppareilSecurise, 
	 * appel� lors d'un new AppareilSecurise(...)
	 * 
	 * @param pNom le nom de l'appareil
	 * @param pMarque la marque de l'appareil securise
	 * @param pModele le modele de l'appareil securise
	 * @param pPuissance la puissance de l'appareil securise
	 * @see AppareilThermostate(String, String, int)
	 */
	public AppareilSecurise(String pNom, String pMarque, String pModele, int pPuissance) {
		// appel du constructeur de la classe AppareilThermostate
		super(pNom, pMarque, pModele, pPuissance);
	}



	/**
	 * @return Returns the securiteEnclenchee.
	 */
	public boolean isSecuriteEnclenchee() {
		return securiteEnclenchee;
	}


	/**
	 * @see java.lang.Object#toString() 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append(", securiteEnclenchee = ").append(securiteEnclenchee);
		return buffer.toString();
	}

	/**
	 * enclenchement de la securite (securiteEnclenchee=true)
	 */
	public void enclencheSecurite() {
		securiteEnclenchee=true;
	}

	/**
	 * desenclenchement de la securite (securiteEnclenchee=false) 
	 * et arret de l'appareil s'il �tait d�marr� 
	 */
	public void desenclencheSecurite() throws Exception {
		// test si l'appareil est en marche
		if (isEnMarche()){
			// arret de l'appareil
			System.out.println("Arret force de l'appareil");
			throw new Exception("Desenclenchement s�curit� impossible, l'appareil est marche");
		}
		securiteEnclenchee=false;
	}

	/**
	 * demarrage de l'appareil securise si la securite est enclenchee
	 * 
	 */
	
	public void demarrer() {
		// Test si la securite est enclenchee 
		if (securiteEnclenchee){
			// la securite est enclenchee => demarrage de l'appareil
			super.demarrer();
		}
		else {
			// la securite n'est pas enclenchee => message d'erreur
			System.out.println("Demarrage impossible, la securite n'est pas enclenchee !");			
		}
		
	}
	
	
	/**
	 * Surcharge de equals
	 * @see AppareilThermostate#equals(Object)
	 */
	@Override
	public boolean equals (Object o){
		// retour vrai si m�me classe, que les attributs de AppareilElectrique sont identiques et que le thermostat est le m�me
		boolean retour = super.equals(o) 
							&& (this.isSecuriteEnclenchee() == ((AppareilSecurise)o).isSecuriteEnclenchee());
			return retour;
	}
	
	@Override
	public String toFile() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(prepareInfos());
		return buffer.toString();
	}
}
