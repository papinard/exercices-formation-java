package com.soprasteria.formation.appareil;


/**
 * classe repr�sentant un appareil �lectrique
 *
 * @author Sopra Group
 * @version 1.0
 */
public class AppareilElectrique implements Controlable {

    /**
     *
     */
    private static final long serialVersionUID = 8132490439087936921L;

	/*
     * Attributs
	 */
    /**
     * nom de l'appareil
     */
    private String nom;

    /**
     * marque de l'appareil
     */
    private String marque;

    /**
     * mod�le de l'appareil
     */
    private String modele;

    /**
     * appareil en marche ?
     */
    protected boolean enMarche;

    /**
     * puissance de l'appareil
     */
    protected int puissance;


    public AppareilElectrique() {
    }

	/*
	 * Methodes
	 */

    /**
     * Constructeur de la classe AppareilElectrique ,
     * appel� lors d'un new AppareilElectrique(...)
     *
     * @param nom       le nom de l'appareil
     * @param marque    la marque de l'appareil électrique
     * @param modele    le modele de l'appareil électrique
     * @param puissance la puissance de l'appareil électrique
     */
    public AppareilElectrique(String nom, String marque, String modele, int puissance) {
        this.nom = nom;
        this.marque = marque;
        this.modele = modele;
        this.puissance = puissance;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setEnMarche(boolean enMarche) {
        this.enMarche = enMarche;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    /**
     * getteur de l'attribut nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * getter de l'attribut marque
     *
     * @return la marque de l'appareil
     */
    public String getMarque() {
        return marque;
    }

    /**
     * getter de l'attribut modele
     *
     * @return le modele de l'appareil
     */
    public String getModele() {
        return modele;
    }

    /**
     * getter de l'attribut puissance
     *
     * @return la puissance de l'appareil �lectrique
     */
    public int getPuissance() {
        return puissance;
    }

    /**
     * getter de l'attribut enMarche
     *
     * @return en marche ?
     */
    public boolean isEnMarche() {
        return enMarche;
    }

    /**
     * methode permettant d'arreter l'appareil
     * (affectation de false � l'attribut enMarche)
     */
    public void arreter() {
        System.out.println("Arrêt de " + nom);
        enMarche = false;
    }

    /**
     * methode permettant de d�marrer l'appareil
     * (affectation de true � l'attribut enMarche)
     */
    public void demarrer() {
        System.out.println("Démarrage de " + nom);
        enMarche = true;
    }


    /**
     * @return la consommation de l'appareil thermostate <ul>
     * <li> puissance si l'appareil est d�marr� </li>
     * <li> 0 si l'appareil est arret� </li>
     * </ul>
     */

    public int getConsommation() {
        if (enMarche) {
            return puissance;
        } else {
            return 0;
        }
    }


    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Nom = ").append(nom);
        buffer.append(", marque = ").append(marque);
        buffer.append(", modele = ").append(modele);
        buffer.append(", puissance = ").append(puissance);
        buffer.append(", enMarche = ").append(enMarche);
        return buffer.toString();

    }


    /**
     * Teste l'égalité de 2 appareils électriques
     *
     * @param o un appareil
     * @return true si les 2 appareils sont égaux, false sinon
     * (test entre l'appareil pass� en param�tre et this)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        boolean retour = false;
        //if (o instance of AppareilElectrique){   // OK mais pas pratique pour le polymorphisme
        if (o.getClass().equals(this.getClass())) {   // test d'égalité de la classe
            if (this.getNom().equals(((AppareilElectrique) o).getNom()) && this.getMarque().equals(((AppareilElectrique) o).getMarque())
                    && this.getModele().equals(((AppareilElectrique) o).getModele())
                    && this.getPuissance() == ((AppareilElectrique) o).getPuissance()
                    ) {
                retour = true;
            }
        }
        return retour;
    }


    /**
     * Cree une chaine de caractères représentant l'appareil électrique.
     * format csv : nom;marque;modele;puissance;type
     *
     * @return
     */
    public String toFile() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(prepareInfos());
        return buffer.toString();
    }

    /**
     * Cree une chaine de caractères représentant l'appareil électrique pour méthode toFile : tout sauf le type.
     * format csv : nom;marque;modele;puissance;
     *
     * @return
     */
    protected String prepareInfos() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.getNom());
        buffer.append(";");
        buffer.append(this.getMarque());
        buffer.append(";");
        buffer.append(this.getModele());
        buffer.append(";");
        buffer.append(this.puissance);
        buffer.append(";");
        return buffer.toString();
    }
}
