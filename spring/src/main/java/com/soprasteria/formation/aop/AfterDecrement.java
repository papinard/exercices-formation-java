package com.soprasteria.formation.aop;

import com.soprasteria.formation.appareil.AppareilThermostate;
import com.soprasteria.formation.appareil.Variator;
import org.aspectj.lang.JoinPoint;

/**
 * Created by lmarchau on 14/06/2016.
 */
public class AfterDecrement {

    public void stopVariator(JoinPoint jp) {
        Variator variator = (Variator) jp.getTarget();
        if (variator instanceof AppareilThermostate && 0 == ((AppareilThermostate) variator).getThermostat()) {
            variator.arreter();
        }
    }


}
